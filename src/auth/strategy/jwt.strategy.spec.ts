import { Test, TestingModule } from '@nestjs/testing';
import { JwtStrategy } from './jwt.strategy';
import { JwtService } from '../service/jwt.service';
import { ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { Auth } from '../auth.entity';

describe('JwtStrategy', () => {
  let strategy: JwtStrategy;
  let jwtService: JwtService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PassportModule],
      providers: [
        JwtStrategy,
        {
          provide: JwtService,
          useValue: {
            validateUser: jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn((key: string) => {
              if (key === 'JWT_SECRET') {
                return 'test_secret';
              }
            }),
          },
        },
      ],
    }).compile();

    strategy = module.get<JwtStrategy>(JwtStrategy);
    jwtService = module.get<JwtService>(JwtService);
  });

  it('should be defined', () => {
    expect(strategy).toBeDefined();
  });

  it('should validate a token successfully', async () => {
    const token = 'test_token';
    const user = { id: 1, email: 'test@example.com' } as Auth;
    jest.spyOn(jwtService, 'validateUser').mockResolvedValue(user);

    const result = await strategy['validate'](token);
    expect(result).toEqual(user);
    expect(jwtService.validateUser).toHaveBeenCalledWith(token);
  });

  it('should throw an error if token is invalid', async () => {
    const token = 'invalid_token';
    jest.spyOn(jwtService, 'validateUser').mockRejectedValue(new Error('Invalid token'));

    await expect(strategy['validate'](token)).rejects.toThrow('Invalid token');
    expect(jwtService.validateUser).toHaveBeenCalledWith(token);
  });
});
