import { HttpException, HttpStatus, ArgumentsHost } from '@nestjs/common';
import { HttpExceptionFilter } from './http-exception.filter';
import { Request, Response } from 'express';

describe('HttpExceptionFilter', () => {
  let filter: HttpExceptionFilter;

  beforeEach(() => {
    filter = new HttpExceptionFilter();
  });

  it('should return the correct response for a BAD_REQUEST exception', () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    } as any as Response;

    const mockRequest = {
      url: '/test-url',
    } as any as Request;

    const mockHost = {
      switchToHttp: jest.fn().mockReturnThis(),
      getResponse: jest.fn().mockReturnValue(mockResponse),
      getRequest: jest.fn().mockReturnValue(mockRequest),
    } as any as ArgumentsHost;

    const exception = new HttpException({ message: 'Validation failed' }, HttpStatus.BAD_REQUEST);

    const result = filter.catch(exception, mockHost);

    expect(result).toEqual({ status: HttpStatus.BAD_REQUEST, error: 'Validation failed' });
  });

  it('should set the response status and return a JSON object for other exceptions', () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    } as any as Response;

    const mockRequest = {
      url: '/test-url',
    } as any as Request;

    const mockHost = {
      switchToHttp: jest.fn().mockReturnThis(),
      getResponse: jest.fn().mockReturnValue(mockResponse),
      getRequest: jest.fn().mockReturnValue(mockRequest),
    } as any as ArgumentsHost;

    const exception = new HttpException('Internal server error', HttpStatus.INTERNAL_SERVER_ERROR);

    filter.catch(exception, mockHost);

    expect(mockResponse.status).toHaveBeenCalledWith(HttpStatus.INTERNAL_SERVER_ERROR);
    expect(mockResponse.json).toHaveBeenCalledWith({
      statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
      timestamp: expect.any(String),
      path: '/test-url',
    });
  });
});
