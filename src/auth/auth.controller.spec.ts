import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './service/auth.service';
import { RegisterRequestDto, LoginRequestDto, ValidateRequestDto } from './auth.dto';
import { HttpStatus } from '@nestjs/common';

describe('AuthController', () => {
  let controller: AuthController;
  let service: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: {
            register: jest.fn(),
            login: jest.fn(),
            validate: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('register', () => {
    it('should call AuthService register', async () => {
      const dto: RegisterRequestDto = { email: 'test@test.com', password: 'password', role: 'user' };
      const result = { status: HttpStatus.CREATED, error: null };
      jest.spyOn(service, 'register').mockResolvedValueOnce(result);

      expect(await controller['register'](dto)).toEqual(result);
      expect(service.register).toHaveBeenCalledWith(dto);
    });
  });

  describe('login', () => {
    it('should call AuthService login', async () => {
      const dto: LoginRequestDto = { email: 'test@test.com', password: 'password' };
      const result = { token: 'token', status: HttpStatus.OK, error: null };
      jest.spyOn(service, 'login').mockResolvedValueOnce(result);

      expect(await controller['login'](dto)).toEqual(result);
      expect(service.login).toHaveBeenCalledWith(dto);
    });
  });

  describe('validate', () => {
    it('should call AuthService validate', async () => {
      const dto: ValidateRequestDto = { token: 'validToken' };
      const result = { status: HttpStatus.OK, error: null, userId: 1 };
      jest.spyOn(service, 'validate').mockResolvedValueOnce(result);

      expect(await controller['validate'](dto)).toEqual(result);
      expect(service.validate).toHaveBeenCalledWith(dto);
    });
  });
});
