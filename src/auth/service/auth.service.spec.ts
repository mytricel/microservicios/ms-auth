import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AuthService } from './auth.service';
import { JwtService } from './jwt.service';
import { Auth } from '../auth.entity';
import { RegisterRequestDto, LoginRequestDto, ValidateRequestDto } from '../auth.dto';
import { HttpStatus } from '@nestjs/common';

describe('AuthService', () => {
  let service: AuthService;
  let repository: Repository<Auth>;
  let jwtService: JwtService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: getRepositoryToken(Auth),
          useClass: Repository,
        },
        {
          provide: JwtService,
          useValue: {
            encodePassword: jest.fn(),
            isPasswordValid: jest.fn(),
            generateToken: jest.fn(),
            verify: jest.fn(),
            validateUser: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    repository = module.get<Repository<Auth>>(getRepositoryToken(Auth));
    jwtService = module.get<JwtService>(JwtService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('register', () => {
    it('should return conflict if email already exists', async () => {
      const dto: RegisterRequestDto = { email: 'test@test.com', password: 'password', role: 'user' };
      jest.spyOn(repository, 'findOne').mockResolvedValueOnce(new Auth());

      const result = await service.register(dto);

      expect(result).toEqual({ status: HttpStatus.CONFLICT, error: ['E-Mail already exists'] });
    });

    it('should create a new user', async () => {
      const dto: RegisterRequestDto = { email: 'test@test.com', password: 'password', role: 'user' };
      jest.spyOn(repository, 'findOne').mockResolvedValueOnce(null);
      jest.spyOn(repository, 'save').mockResolvedValueOnce(new Auth());
      jest.spyOn(jwtService, 'encodePassword').mockReturnValue('encodedPassword');

      const result = await service.register(dto);

      expect(result).toEqual({ status: HttpStatus.CREATED, error: null });
    });
  });

  describe('login', () => {
    it('should return not found if email does not exist', async () => {
      const dto: LoginRequestDto = { email: 'test@test.com', password: 'password' };
      jest.spyOn(repository, 'findOne').mockResolvedValueOnce(null);

      const result = await service.login(dto);

      expect(result).toEqual({ status: HttpStatus.NOT_FOUND, error: ['E-Mail not found'], token: null });
    });

    it('should return not found if password is invalid', async () => {
      const dto: LoginRequestDto = { email: 'test@test.com', password: 'password' };
      const auth = new Auth();
      auth.password = 'encodedPassword';
      jest.spyOn(repository, 'findOne').mockResolvedValueOnce(auth);
      jest.spyOn(jwtService, 'isPasswordValid').mockReturnValueOnce(false);

      const result = await service.login(dto);

      expect(result).toEqual({ status: HttpStatus.NOT_FOUND, error: ['Password wrong'], token: null });
    });

    it('should return token if login is successful', async () => {
      const dto: LoginRequestDto = { email: 'test@test.com', password: 'password' };
      const auth = new Auth();
      auth.password = 'encodedPassword';
      jest.spyOn(repository, 'findOne').mockResolvedValueOnce(auth);
      jest.spyOn(jwtService, 'isPasswordValid').mockReturnValueOnce(true);
      jest.spyOn(jwtService, 'generateToken').mockReturnValue('token');

      const result = await service.login(dto);

      expect(result).toEqual({ token: 'token', status: HttpStatus.OK, error: null });
    });
  });

  describe('validate', () => {
    it('should return forbidden if token is invalid', async () => {
      const dto: ValidateRequestDto = { token: 'invalidToken' };
      jest.spyOn(jwtService, 'verify').mockResolvedValueOnce(null);

      const result = await service.validate(dto);

      expect(result).toEqual({ status: HttpStatus.FORBIDDEN, error: ['Token is invalid'], userId: null });
    });

    it('should return conflict if user is not found', async () => {
      const dto: ValidateRequestDto = { token: 'validToken' };
      const decoded = new Auth();
      jest.spyOn(jwtService, 'verify').mockResolvedValueOnce(decoded);
      jest.spyOn(jwtService, 'validateUser').mockResolvedValueOnce(null);

      const result = await service.validate(dto);

      expect(result).toEqual({ status: HttpStatus.CONFLICT, error: ['User not found'], userId: null });
    });

    it('should return user id if token is valid', async () => {
      const dto: ValidateRequestDto = { token: 'validToken' };
      const decoded = new Auth();
      decoded.id = 1;
      jest.spyOn(jwtService, 'verify').mockResolvedValueOnce(decoded);
      jest.spyOn(jwtService, 'validateUser').mockResolvedValueOnce(decoded);

      const result = await service.validate(dto);

      expect(result).toEqual({ status: HttpStatus.OK, error: null, userId: 1 });
    });
  });
});
