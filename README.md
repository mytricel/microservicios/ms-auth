<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">Microservicio de autentificación para la aplicación myTricel.</p>    


## Instalación de dependencias

```bash
$ npm i @nestjs/microservices @nestjs/typeorm @nestjs/jwt @nestjs/passport passport passport-jwt typeorm pg class-transformer class-validator bcryptjs
$ npm i -D @types/node @types/passport-jwt ts-proto
```

## Scripts para los protos

Estos scripts se añaden en el archivo `package.json` para poder generar los archivos `.pb`

```bash
"proto:install": "npm i git+https://github.com/YOUR_USERNAME/grpc-nest-proto.git",
"proto:auth": "protoc --plugin=node_modules/.bin/protoc-gen-ts_proto -I=./node_modules/grpc-nest-proto/proto --ts_proto_out=src/auth/ node_modules/grpc-nest-proto/proto/auth.proto --ts_proto_opt=nestJs=true --ts_proto_opt=fileSuffix=.pb"
```

Luego se crean los archivos con este comando

```bash
$ npm run proto:install && npm run proto:auth
```

## Base de datos en Docker

Creación de la bd en el contenedor y el logeo a esta.

```bash
$ docker run -d --name arqui -e POSTGRES_USER=root -e POSTGRES_PASSWORD=root -e POSTGRES_DB=micro_auth -p 5432:5432 postgres
$ docker exec -it arqui psql -U root -d micro_auth
```


## Ejecutar la app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

### Instalación de dependencias
```bash
$ npm install --save-dev jest @types/jest ts-jest @nestjs/testing
```

### Configuracion de Archivo Jest

Se agrega este archivo en la raiz del proyecto.

```bash
module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: 'src',
  testRegex: '.*\\.spec\\.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  collectCoverageFrom: [
    '**/*.(t|j)s',
  ],
  coverageDirectory: '../coverage',
  testEnvironment: 'node',
};
```

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

